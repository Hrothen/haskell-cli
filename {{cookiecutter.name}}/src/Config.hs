-- |
-- Module      :  Config
-- Copyright   :  {{cookiecutter.name}} {{cookiecutter.year}}
-- License     :  {{cookiecutter.license}}
--
-- Maintainer  :  {{cookiecutter.email}}
-- Stability   :  experimental
-- Portability :  unknown
--
-- Module for handling configuration files
--
{-# LANGUAGE OverloadedStrings #-}
module Config
    ( Config(..)
    , defaultConfig
    , readConfigFile
    , loadOrCreateConfigFile
    ) where

import Data.Text(Text(..))
import qualified Data.Text.IO as TIO
import System.Directory(XdgDirectory(..), findFile, getXdgDirectory)
import System.FilePath(FilePath, (</>))


data Config = Config
  { configStr :: Text
  } deriving (Show, Read, Eq)


defaultConfig :: Config
defaultConfig = Config "A string"


readConfigFile :: FilePath -> IO Config
readConfigFile path = Config <$> TIO.readFile path


getConfigPath :: String -> IO FilePath
getConfigPath programName = getXdgDirectory XdgConfig programName


loadOrCreateConfigFile :: String -> IO (Config)
loadOrCreateConfigFile programName = do
  configPath <- getConfigPath programName
  configFile <- findFile [configPath] "config"
  case configFile of
    Just f  -> readConfigFile f
    Nothing -> do
      writeConfigFile (configPath </> "config")
      return defaultConfig


writeConfigFile :: FilePath -> IO ()
writeConfigFile path = putStrLn $
  "Were this implemented, a config file would be created at " ++ path
